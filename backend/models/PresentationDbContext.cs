using Microsoft.EntityFrameworkCore;

namespace Seavus.ReactPresentation.Models
{
    public class PresentationDbContext : DbContext
    {
        public PresentationDbContext(DbContextOptions<PresentationDbContext> options): base(options)
        {
            
        }
        public DbSet<Employee> Employees { get; set; }
    }
}
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Seavus.ReactPresentation.Models;
using Microsoft.Extensions.Configuration;

namespace Seavus.ReactPresentation
{
    public partial class Startup
    {
        public IConfiguration Configuration { get; set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<PresentationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Default")));
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();


            app.UseCors(builder => builder.WithOrigins("http://localhost:5001").AllowAnyHeader());

            loggerFactory.AddDebug();
            loggerFactory.AddConsole();

            app.UseMvc();

        }
    }

}
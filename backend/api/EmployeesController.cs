using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Seavus.ReactPresentation.Models;

namespace Seavus.ReactPresentation.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : Controller
    {
        private readonly PresentationDbContext _dbContext;
        public EmployeesController(PresentationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public string Ping()
        {
            return "Hi";
        }

        [HttpGet]
        [Route("all")]
        public List<Employee> GetAll()
        {
            return _dbContext.Employees.ToList();
        }

        [HttpGet]
        [Route("{id}")]
        public Employee Get(int id){
            return _dbContext.Employees.FirstOrDefault(c=>c.Id == id);
        }

        [HttpPost]
        [Route("create")]
        public void Create([FromBody] Employee employee)
        {
            _dbContext.Employees.Add(employee);
            _dbContext.SaveChangesAsync();
        }

        

    }

}
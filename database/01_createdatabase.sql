if db_id('PresentationDb') is null
begin
create database PresentationDb
end
go
begin
use PresentationDb
if not exists (select * from sysobjects where name='Employees' and xtype='U')
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[BirthDate] [datetime] NOT NULL)

end
go
begin
INSERT INTO [dbo].[Employees]
           ([FirstName]
           ,[LastName]
           ,[BirthDate])
     VALUES
           ('Milos', 'Ostojic', '1985-02-23'),
		   ('Vladimir', 'Sovric', '1975-01-01')
		   end
GO



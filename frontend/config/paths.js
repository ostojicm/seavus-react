var path = require('path');
var fs = require('fs');

var appDirectory = fs.realpathSync(process.cwd());
var resolveApp = function (relativePath) {
    return path.resolve(appDirectory, relativePath);
};

module.exports = {
    appHtml : resolveApp('index.html')
}
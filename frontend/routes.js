import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import EmployeeList from './containers/employees/employeelist'
import Employee from './containers/employees/employee'
import Employees from './containers/employees/index'

export default (
    <Route path="/" component={App}>
        <Route path="/employees" component={Employees}>
            <IndexRoute component={EmployeeList} />
            <Route path="/employees/:mode(/:employeeId)" component={Employee} />
        </Route>
    </Route>

)
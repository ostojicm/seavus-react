import React, { Component, PropTypes } from 'react'
import Item from './item'

export default class Table extends Component {
    static propTypes = {
        employees: PropTypes.array.isRequired
    };

    render() {
        const { employees } = this.props;
        return (
            <table>
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Birth date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {employees.map(employee =>
                        <Item key={employee.id} employee={employee} />
                    )}
                </tbody>
            </table>
        );
    }
}
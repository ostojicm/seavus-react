import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

export default class Item extends Component {
    static propTypes = {
        employee: PropTypes.shape({
            id: PropTypes.number.isRequired,
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired,
            birthDate: PropTypes.string.isRequired
        })
    };

    formatStatus(state) {
        if (state == 'changed')
            return 'fi-pencil';
        if (state == 'added')
            return 'fi-plus';
        if (state == 'deleted')
            return 'fi-x';

        return 'fi-list';
    }

    render() {
        const { employee } = this.props;
        return (
            <tr>
                <td>{employee.firstName}</td>
                <td>{employee.lastName}</td>
                <td>{employee.birthDate}</td>
                <th><i className={this.formatStatus(employee.state)}></i></th>
                <td>
                    <Link to={"employees/view/" + employee.id} className="tiny button"><i className="fi-page"></i></Link>
                    <Link to={"employees/edit/" + employee.id} className="tiny button"><i className="fi-page-edit"></i></Link>
                </td>
            </tr>);
    }
}
import React, { Component, PropTypes } from 'react';

export default class EmployeeEditor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employee: this.initializeDefaultEmployee(props)
        }
        this.isReadOnly = !this.isEditable();
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this)
        this.handleLastNameChange = this.handleLastNameChange.bind(this)
        this.handleBirthDayChange = this.handleBirthDayChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    generateNewId(){
        let rand = Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER) + 1)
        return 0-rand;
    }

    initializeDefaultEmployee(props){
        return props.employee || { id: this.generateNewId(), firstName: '', lastName: '', birthDate: ''}
    }

    static propTypes = {
        employee: PropTypes.object,
        submitEmployee: PropTypes.func.isRequired,
        mode: PropTypes.string.isRequired
    }

    componentWillReceiveProps(nextProps){
        let nextEmployee = this.initializeDefaultEmployee(nextProps)
        this.setState({employee: nextEmployee})
    }

    updateState(property, value) {
        const {employee} = this.state;
        employee[property] = value;
        this.setState({ employee });
    }

    handleFirstNameChange(event) {
        this.updateState('firstName', event.target.value);
    }

    handleLastNameChange(event) {
        this.updateState('lastName', event.target.value)
    }

    handleBirthDayChange(event) {
        this.updateState('birthDate', event.target.value);
    }

    handleSubmit(event) {
        event.preventDefault();
        const {employee} = this.state
        this.props.submitEmployee(employee, this.props.mode)

    }

    isEditable(){
        const { mode } = this.props
        return mode != 'view';
    }

    render() {
        const { employee } = this.state
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="medium-6 columns">
                        <label>First name:
                        <input name="firstName" type="text" readOnly={this.isReadOnly} value={employee.firstName} onChange={this.handleFirstNameChange}></input>
                        </label>
                    </div>
                    <div className="medium-6 columns">
                        <label>Last name:
                        <input name="lastName" type="text" readOnly={this.isReadOnly} value={employee.lastName} onChange={this.handleLastNameChange}></input>
                        </label>
                    </div>
                    <div className="medium-6 columns">
                        <label>Birth date:
                        <input name="lastName" type="text" readOnly={this.isReadOnly} value={employee.birthDate} onChange={this.handleBirthDayChange}></input>
                        </label>
                    </div>
                    {this.isEditable() &&
                        <div className="medium-6 columns">
                            <input name="save" type="submit"></input>
                        </div>}
                </div>
            </form>
        );
    }
}

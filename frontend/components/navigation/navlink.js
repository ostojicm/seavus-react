import React, { PropTypes } from "react";
import { Link } from "react-router";

export default React.createClass({
    displayName : "NavLink",
    propTypes : {
        to: PropTypes.string.isRequired
    },
    contextTypes: {
        router: PropTypes.object
    },
    render() {
        let isActive = this.context.router.isActive(this.props.to, false);

        return (
            <li className={isActive ? "active" : ""}>
                <Link {...this.props} activeClassName="active" />
            </li>
        );
    }
});
import React, { Component } from 'react';
import { Link } from 'react-router';
import NavLink from './navlink';

export default class Navigation extends Component {
    render() {
        return (
            <div className="top-bar">
                <div className="top-bar-left">
                    <ul className="menu">
                        <li className="top-bar-logo">
                            <Link to="/">
                                <img src="content/logo.png" />
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="top-bar-right">
                    <ul className="menu">
                        <NavLink to="employees">Employees</NavLink>                        
                    </ul>
                </div>
            </div>
        );
    }
} 
var path = require('path');
var webpack = require('webpack');
//var HtmlWebpackPlugin = require('html-webpack-plugin');
var paths = require('./config/paths');

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    './index'
  ],
  eslint: {
    configFile: '.eslintrc.json'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    preLoaders: [
      {test: /\.js$/, loader: "eslint-loader", exclude: /node_modules/}
    ],
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'babel' ],
        exclude: /node_modules/,
        include: __dirname
      }
    ]
  }
}

// module.exports = {
//     devtools: 'eval',
//     entry: [
//         'webpack-dev-server/client?http://localhost:5001',
//         'webpack/hot/only-dev-server',
//         'react-hot-loader/patch',
//         './index'
//     ],

//     output: {
//         path: path.join(__dirname, 'dist'),
//         filename: 'bundle.js',
//         publicPath: '/static/'
//     },
//     plugins: [
//         new HtmlWebpackPlugin({
//             inject: 'body',
//             template: paths.appHtml
//         }),
//         new webpack.HotModuleReplacementPlugin()
//     ],
//     resolve: {
//         // alias: {
//         //     'redux-devtools/lib': path.join(__dirname, '..', '..', 'src'),
//         //     'redux-devtools': path.join(__dirname, '..', '..', 'src'),
//         //     'react': path.join(__dirname, 'node_modules', 'react')
//         // },
//         extensions: ['', '.js']
//     },
//     resolveLoader: {
//         'fallback': path.join(__dirname, 'node_modules')
//     },
//     module: {
//         loaders: [{
//             test: /\.js$/,
//             loaders: ['babel'],
//             exclude: /node_modules/,
//             include: __dirname
//         }, {
//             test: /\.js$/,
//             loaders: ['babel'],
//             include: path.join(__dirname, '..', '..', 'src')
//         }, {
//             test: /\.css?$/,
//             loaders: ['style', 'raw'],
//             include: __dirname
//         }]
//     }
// };

import { combineReducers } from 'redux'
import * as ActionTypes from '../actions/employeeActions'
import merge from 'lodash/merge'
import concat from 'lodash/concat'
import filter from 'lodash/filter'
import remove from 'lodash/remove'
import { LOCATION_CHANGE } from 'react-router-redux'

const initialEmployeesState = {
    fetching: false,
    fetched: false,
    data: []
};

const initialEmployeeState = {
    fetching: false,
    fetched: false,
    data: null
}

const server = (state = initialEmployeesState, action) => {
    if (action.type === ActionTypes.EMPLOYEES_SUCCESS) {
        const newState = merge({}, state, { data: [...action.response.payload] })
        return newState;
    }

    if (action.type === ActionTypes.EMPLOYEE_EDIT) {
        const newState = merge({}, state);
        remove(newState.data, (item)=> { return item.id == action.employee.id})
        
        return newState
    }

    return state;
}

const added = (state = [], action) => {
    if (action.type == ActionTypes.EMPLOYEE_CREATE) {
        const addedEmployee = { ...action.employee, state: 'added' }
        const newState = concat(state, [addedEmployee])
        return newState;
    }

    if (action.type === ActionTypes.EMPLOYEE_EDIT) {
        if (action.employee.state == 'added') {
            return state.map(i => {
                return i.id == action.employee.id ? action.employee : i
            });
        }
    }
    return state;
}

const changed = (state = [], action) => {
    if (action.type === ActionTypes.EMPLOYEE_EDIT) {
        //if employee state is already added leave it        
        if (action.employee.state == 'added')
            return state;

        const editedEmployee = { ...action.employee, state: 'changed' }
        return concat(state, [editedEmployee])

    }
    return state;
}

const deleted = (state = [], action) => {
    if (action.type === ActionTypes.EMPLOYEE_DELETE) {
        const deletedEmployee = { ...action.employee, state: 'deleted' }
        const newState = concat(state, [deletedEmployee])
        return newState;
    }

    //TODO: remove naive impl
    return state;
}

const current = (state = initialEmployeeState, action) => {
    if (action.type === ActionTypes.EMPLOYEE_SUCCESS) {
        const newState = merge({}, state, { data: action.response.payload })
        return newState;
    }

    if (action.type === ActionTypes.CACHED_EMPLOYEE) {
        const newState = merge({}, state, { data: action.payload })
        return newState;
    }
    return state;
}

const employees = combineReducers({
    server,
    current,
    added,
    changed,
    deleted
})

export default employees


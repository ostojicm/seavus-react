import { combineReducers } from 'redux';
import employees from './employees';
import { routerReducer as routing } from 'react-router-redux'

const rootReducer = combineReducers({
    employees,
    routing
});

export default rootReducer;
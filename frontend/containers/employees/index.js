import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'

export default class Employees extends Component {
    static propTypes = {
        children: PropTypes.object
    }
    render() {
        return (
            <div>
                {this.props.children}
            </div>);

    }
}
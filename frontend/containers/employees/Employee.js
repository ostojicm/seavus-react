import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import EmployeeEditor from '../../components/employees/editor'
import { loadEmployee, editEmployee, navigateToStart } from '../../actions/employeeActions'


function loadData(props) {
    const { id } = props;
    if (id)
        props.loadEmployee(id);
}

class Employee extends Component {
    constructor(props) {
        super(props)

    }

    static propTypes = {
        employee: PropTypes.object,
        editEmployee: PropTypes.func.isRequired,
        finish: PropTypes.func.isRequired,
        mode: PropTypes.string.isRequired
    }

    componentWillMount() {
        loadData(this.props);
    }

    handleSubmit(data, mode) {
        this.props.editEmployee(data, mode);
        this.props.finish();
    }

    render() {
        let { employee, mode } = this.props

        return (
            <EmployeeEditor employee={employee} submitEmployee={this.handleSubmit.bind(this)} mode={mode} />
        )
    }
}

export const MODE_EDIT = "edit"
export const MODE_CREATE = "create"
export const MODE_DELETE = "delete"
export const MODE_VIEW = "view"

const mapStateToProps = (state, ownProps) => {
    const mode = ownProps.params.mode

    const id = parseInt(ownProps.params.employeeId);

    const { employees: { current: { data: employee } }} = state;
    return {
        id,
        employee: mode === MODE_CREATE ? null : employee,
        mode
    }
}

export default connect(mapStateToProps, { loadEmployee, editEmployee, finish: navigateToStart })(Employee)


import React, { Component, PropTypes } from 'react'
import Table from '../../components/employees/list/table';
import { loadEmployees } from '../../actions/employeeActions'
import { connect } from 'react-redux';
import { Link } from 'react-router'

function loadData(props) {
    props.loadEmployees();
}

class EmployeeList extends Component {
    constructor() {
        super();

    }

    componentWillMount() {
        loadData(this.props);
    }

    static propTypes = {
        employees: PropTypes.array.isRequired
    }

    render() {
        return (
            <div>
                <Table employees={this.props.employees} />
                <div className="row">
                    <div className="small-3 small-centered columns text-center">
                        <Link className="button" to="/employees/create"><i className="fi-plus"></i></Link>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { employees : {added, changed, deleted, server: { data: server} } } = state
    let mergedEmpl = [...server, ...changed, ...added, ...deleted]
    
    return {
        employees : mergedEmpl
    }

}

export default connect(mapStateToProps, { loadEmployees })(EmployeeList)
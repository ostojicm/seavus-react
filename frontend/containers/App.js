import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Navigation from '../components/navigation'

class App extends Component {
    static propTypes = {
        children : PropTypes.object
    }
    render() {
        return (
            <div>
                <Navigation />
                {this.props.children}
            </div>
        );
    }
}

export default connect()(App)
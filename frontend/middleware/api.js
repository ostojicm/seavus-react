import axios from 'axios'

export const API_ENDPOINT = Symbol("Call REST API");
const API_ROOT = 'http://localhost:5000/api/'

function callApi(endpoint) {
    const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint
    return axios.get(fullUrl)
        .then(response => {
            const data = Object.assign({}, { payload: response.data })            
            return data;
        })
        .catch(error => {
            return Promise.reject(error);
        });
    // .then(response => response.json().then(json => { json, response }))
    // .then(({ json, response }) => {
    //     if (!response.ok)
    //         return Promise.reject(json);

    //     return Object.assign({}, { employees: json });
    // });
}

const api = (store) => (next) => (action) => {
   const apiPath = action[API_ENDPOINT];

    if (typeof apiPath === 'undefined') {
        return next(action)
    }

    let { endpoint } = apiPath;
    const { types, schema } = apiPath;

    if (typeof endpoint === 'function') {
        endpoint = endpoint(store.getState())
    }

    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.')
    }

    function actionWith(data) {
        const finalAction = Object.assign({}, action, data)
        delete finalAction[API_ENDPOINT]
        return finalAction;
    }

    const [requestType, successType, failureType] = types
    next(actionWith({ type: requestType }))

    return callApi(endpoint).then(response => next(actionWith({
        response,
        type: successType,
        schema
    })),
        error => next(actionWith({ type: failureType, error: error.message || 'Failed to load data' }))
    )

}

export default api
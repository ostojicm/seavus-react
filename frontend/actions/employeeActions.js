import { API_ENDPOINT } from '../middleware/api'
import { push } from 'react-router-redux'
import concat from 'lodash/concat'
import omitBy from 'lodash/omitBy'
import isUndefined from 'lodash/isUndefined'

export const EMPLOYEES_REQUEST = 'EMPLOYEES_REQUEST'
export const EMPLOYEES_SUCCESS = 'EMPLOYEES_SUCCESS'
export const EMPLOYEES_FAILURE = 'EMPLOYEES_FAILURE'

export const EMPLOYEE_REQUEST = 'EMPLOYEE_REQUEST'
export const EMPLOYEE_SUCCESS = 'EMPLOYEE_SUCCESS'
export const EMPLOYEE_FAILURE = 'EMPLOYEE_FAILURE'

export const EMPLOYEE_CREATE = 'EMPLOYEE_CREATE'
export const EMPLOYEE_EDIT = 'EMPLOYEE_EDIT'
export const EMPLOYEE_DELETE = 'EMPLOYEE_DELETE'

export const CACHED_EMPLOYEE = 'CACHED_EMPLOYEE'


function fetchEmployees() {
    return {
        [API_ENDPOINT]: {
            types: [EMPLOYEES_REQUEST, EMPLOYEES_SUCCESS, EMPLOYEES_FAILURE],
            endpoint: `employees/all`,
            schema: 'employeeList'
        }
    }
}

function fetchEmployee(id) {
    return {
        [API_ENDPOINT]: {
            types: [EMPLOYEE_REQUEST, EMPLOYEE_SUCCESS, EMPLOYEE_FAILURE],
            endpoint: `employees/${id}`,
            schema: 'employee'
        }
    }
}

const findUnchanged = (state, id) => {
    return state.employees.server.data.find(e => e.id == id);
}
const findChanged = (state, status, id) => {
    return state.employees[status].find(e => e.id == id);
}
const findInCache = (state, id) => {
    return concat([findUnchanged(state, id)], [findChanged(state, 'added', id)], [findChanged(state, 'deleted', id)], findChanged(state, 'changed', id)).find(c => c ? c.id == id : null)
}
export function loadEmployee(id) {
    if (!id)
        throw Error("Parameter ID must have value");
    return (dispatch, getState) => {
        const employee = findInCache(getState(), id)
        if (employee) {
            return (dispatch({ type: CACHED_EMPLOYEE, payload: employee }))
        }

        return (dispatch(fetchEmployee(id)))
    }
}

const hasLocalModifications = (state) => {
    return state.employees.server.data.length > 0
        || state.employees.added.length > 0
        || state.employees.changed.length > 0
        || state.employees.deleted.length > 0
}

export function loadEmployees() {
    return (dispatch, getState) => {
        if (hasLocalModifications(getState()))
            return null;
        return dispatch(fetchEmployees())
    };
}

export function editEmployee(employee, mode) {
    return (dispatch, getState) => {
        if (mode === 'edit')
            return dispatch({
                type: EMPLOYEE_EDIT,
                employee
            })

        if (mode === 'create')
            return dispatch({
                type: EMPLOYEE_CREATE,
                employee
            })

        if (mode === 'delete')
            return dispatch({
                type: EMPLOYEE_DELETE,
                employee
            })

        return null;
    }
}

export function navigateToStart() {
    return (dispatch) => {
        return dispatch(push('/employees'))
    }
}

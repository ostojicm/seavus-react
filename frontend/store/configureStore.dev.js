import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import api from '../middleware/api';
import rootReducer from '../reducers';
import DevTools from '../containers/DevTools';
import { routerMiddleware } from 'react-router-redux'

export default function configureStore(browserHistory) {
  const enhancer = compose(applyMiddleware(thunk, api, createLogger(), routerMiddleware(browserHistory) ), DevTools.instrument());

  const store = createStore(
    rootReducer,
    enhancer
  );

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default)
    );
  }

  return store;
}